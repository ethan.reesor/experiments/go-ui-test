package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"os"
	"unsafe"

	"gitlab.com/firelizzard/go-app"
	"gitlab.com/firelizzard/go-app/cgo"
	"gitlab.com/firelizzard/go-app/darwin"
)

//go:generate go run ./gen/ui

func platformSetup(appSettings *app.Settings, windowSettings *app.WindowSettings) *app.Window {
	xibRaw, err := base64.RawStdEncoding.DecodeString(xibEncoded)
	if err != nil {
		panic(fmt.Errorf("failed to decode embedded nib: %v", err))
	}

	xib := darwin.LoadNibFromData(nil, xibRaw).Autorelease()
	if !xib.Ok() {
		panic("failed to load embedded nib")
	}

	ok := darwin.LoadNibFromData(nil, xibRaw).InstantiateAndRelease(nil, func(xib *darwin.Nib) {
		menu, ok := xib.FindMenu(cgo.GlobalScope)
		if !ok {
			fmt.Fprintf(os.Stderr, "Failed to locate menu in embedded XIB\n")
			app.Terminate()
			return
		}
		appSettings.Menu = menu

		prefs := menu.GetItem(0).Submenu().GetItem(2)
		if prefs.Title() != "Preferences…" {
			log.Fatalf("Menu 0 item 2 is not preferences: %q", prefs.Title())
		}

		prefs.SetAction(func(unsafe.Pointer) {
			fmt.Fprintf(os.Stderr, "No preferences!")
		})
	})
	if !ok {
		fmt.Fprintf(os.Stderr, "Failed to load embedded XIB\n")
		app.Terminate()
	}

	if !xib.Instantiate(nil) {
		panic("failed to instantiate embedded nib")
	}

	app.Start(appSettings)
	window := app.NewWindow(windowSettings)

	window.SetDelegate(&darwin.WindowDelegate{
		WillClose: func(unsafe.Pointer) { app.Stop() }, // BUG: broken
	})

	return window
}
