run(() => {
// const init = (() => {
    document.body.innerHTML = ''

    const canvas = document.createElement('canvas')
    document.body.appendChild(canvas)

    const main = document.createElement('main')
    main.id = 'container'
    document.body.appendChild(main)

    const script = document.createElement('textarea')
    main.appendChild(script)

    script.value = `// draw.cuboid()\ndraw.ellipsoid().scale(1, 2, 1)\ndraw.cuboid().translate(2, 0, 0).scale(1/3, 1/3, 1)`

    compile()
    script.addEventListener('keyup', compile)

    try {
        render(canvas)
    } catch (err) {
        alert(err)
    }

    function compile() {
        script.classList.remove('error')

        try {
            const fn = new Function('draw', script.value)
            fn(new Drawer())

            window.model = (...args) => {
                try {
                    fn(...args)
                } catch (err) {
                    console.error(err)
                }
            }
        } catch (err) {
            script.classList.add('error')
            console.error(err)
        }
    }
})