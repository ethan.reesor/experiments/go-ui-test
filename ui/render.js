class Drawer {
    constructor(gl, main) {
        Object.assign(this, { gl, main })
        this.objects = []
    }

    render() {
        for (const o of this.objects)
            o.render()
    }

    cuboid(pretty) {
        return new Entity(this.objects, model => {
            const { gl, main } = this
            if (pretty)
                Entity.renderSimple(gl, main, model, main.buffer.pretty)
            else
                Entity.renderSimple(gl, main, model, main.buffer.cube)
        })
    }

    ellipsoid() {
        return new Entity(this.objects, model => {
            const { gl, main } = this
            Entity.renderSimple(gl, main, model, main.buffer.sphere)
        })
    }
}

class Entity {
    constructor(o, fn) {
        const model = mat4.create()
        Object.assign(this, { fn, model })

        o.push(this)
    }

    static renderSimple(gl, main, model, buffer) {
        const normal = mat4.invert(mat4.create(), model)
        mat4.transpose(normal, normal)

        gl.bindBuffer(gl.ARRAY_BUFFER, buffer.position)
        gl.vertexAttribPointer(main.attrib.vPosition, 3, gl.FLOAT, false, 0, 0)
        gl.enableVertexAttribArray(main.attrib.vPosition)

        gl.bindBuffer(gl.ARRAY_BUFFER, buffer.color)
        gl.vertexAttribPointer(main.attrib.vColor, 4, gl.FLOAT, false, 0, 0)
        gl.enableVertexAttribArray(main.attrib.vColor)

        gl.bindBuffer(gl.ARRAY_BUFFER, buffer.normal)
        gl.vertexAttribPointer(main.attrib.vNormal, 3, gl.FLOAT, false, 0, 0)
        gl.enableVertexAttribArray(main.attrib.vNormal)

        gl.uniformMatrix4fv(main.uniform.xNormal, false, normal)
        gl.uniformMatrix4fv(main.uniform.xModel, false, model)

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer.index)
        gl.drawElements(gl.TRIANGLES, buffer.count, gl.UNSIGNED_SHORT, 0)
    }

    render() {
        this.fn(this.model)
    }

    scale(x, y, z) {
        const { model } = this
        mat4.scale(model, model, vec3.fromValues(x, y, z))
        return this
    }

    scaleUniform(s) {
        this.scale(s, s, s)
        return this
    }

    translate(x, y, z) {
        const { model } = this
        mat4.translate(model, model, vec3.fromValues(x, y, z))
        return this
    }

    rotate(x, y, z, theta) {
        const { model } = this
        mat4.rotate(model, model, theta, vec3.fromValues(x, y, z))
        return this
    }
}

render = (function() {

const Colors = {
    R: [1,  0,  0,  1],
    G: [0,  1,  0,  1],
    B: [0,  0,  1,  1],
    W: [1,  1,  1,  1],

    C: [0,  1,  1,  1],
    M: [1,  0,  1,  1],
    Y: [1,  1,  0,  1],
    K: [0,  0,  0,  1],
}

// TODO shaders from https://github.com/xdsopl/webgl/blob/master/cubemap.html
return render

async function render(canvas) {
    const gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl')
    if (!gl) throw new Error('Unable to initialize WebGL')

    const M = await main(gl)
    const S = await skybox(gl)

    let last = 0
    requestAnimationFrame(render)

    function render(time) {
        time *= 0.001 // ms -> s
        const duration = time - last
        last = time

        const width = canvas.clientWidth
        const height = canvas.clientHeight
        if (canvas.width != width || canvas.height != height) {
            Object.assign(canvas, {width, height})
            gl.viewport(0, 0, width, height)
        }

        const { up, pos, look } = camera.update(duration)

        drawScene(gl, M, S, up, pos, look)
        requestAnimationFrame(render)
    }
}

async function load(url) {
    const resp = await fetch("/shader/model-fragment.glsl")
    return await resp.text()
}

async function main(gl) {
    const program = createShaderProgram(gl, await load("/shader/model-vertex.glsl"), await load("/shader/model-fragment.glsl"))

    return {
        program,
        buffer: {
            cube: cubeBuffers(gl),
            pretty: cubeBuffers(gl, true),
            sphere: sphereBuffers(gl),
        },
        attrib: {
            vPosition: gl.getAttribLocation(program, 'avPosition'),
            vNormal: gl.getAttribLocation(program, 'avNormal'),
            vColor: gl.getAttribLocation(program, 'avColor'),
        },
        uniform: {
            xNormal: gl.getUniformLocation(program, 'uxNormal'),
            xModel: gl.getUniformLocation(program, 'uxModel'),
            xView: gl.getUniformLocation(program, 'uxView'),
            xProjection: gl.getUniformLocation(program, 'uxProjection'),
            vAmbientLight: gl.getUniformLocation(program, 'uvAmbientLight'),
            vDirectionalLight: gl.getUniformLocation(program, 'uvDirectionalLight'),
            vLightDirection: gl.getUniformLocation(program, 'uvLightDirection'),
        },
    }
}

function cubeBuffers(gl, pretty) {
    const { R, G, B, W, C, M, Y, K } = Colors

    const r = 1/2
    const vposition = [
        // front
        -r, -r, +r,
        +r, -r, +r,
        +r, +r, +r,
        -r, +r, +r,

        // back
        -r, -r, -r,
        -r, +r, -r,
        +r, +r, -r,
        +r, -r, -r,

        // top
        -r, +r, -r,
        -r, +r, +r,
        +r, +r, +r,
        +r, +r, -r,

        // bottom
        -r, -r, -r,
        +r, -r, -r,
        +r, -r, +r,
        -r, -r, +r,

        // right
        +r, -r, -r,
        +r, +r, -r,
        +r, +r, +r,
        +r, -r, +r,

        // left
        -r, -r, -r,
        -r, -r, +r,
        -r, +r, +r,
        -r, +r, -r,
    ]
    const vcolor = [
        W, C, B, M, // front
        Y, R, K, G, // back
        R, M, B, K, // top
        Y, G, C, W, // bottom
        G, K, B, C, // right
        Y, W, M, R, // left
    ]
    const vindex = [
        0,  1,  2,    0,  2,  3,  // front
        4,  5,  6,    4,  6,  7,  // back
        8,  9,  10,   8,  10, 11, // top
        12, 13, 14,   12, 14, 15, // bottom
        16, 17, 18,   16, 18, 19, // right
        20, 21, 22,   20, 22, 23, // left
    ]
    const vnormal = [
        // Front
        +0, +0, +1,
        +0, +0, +1,
        +0, +0, +1,
        +0, +0, +1,

        // Back
        +0, +0, -1,
        +0, +0, -1,
        +0, +0, -1,
        +0, +0, -1,

        // Top
        +0, +1, +0,
        +0, +1, +0,
        +0, +1, +0,
        +0, +1, +0,

        // Bottom
        +0, -1, +0,
        +0, -1, +0,
        +0, -1, +0,
        +0, -1, +0,

        // Right
        +1, +0, +0,
        +1, +0, +0,
        +1, +0, +0,
        +1, +0, +0,

        // Left
        -1, +0, +0,
        -1, +0, +0,
        -1, +0, +0,
        -1, +0, +0
    ]

    if (!pretty)
        for (const i in vcolor)
            vcolor[i] = [0.5, 0.5, 0.5, 1]

    const position = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, position)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vposition), gl.STATIC_DRAW)

    const color = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, color)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vcolor.flat()), gl.STATIC_DRAW)

    const index = gl.createBuffer()
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index)
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vindex), gl.STATIC_DRAW)

    const normal = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, normal)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vnormal), gl.STATIC_DRAW)

    return { position, color, index, normal, count: 36 }
}

function sphereBuffers(gl) {
    // const { R, G, B, W, C, M, Y, K } = Colors

    const vposition = []
    const vcolor = []
    const vindex = []
    const vnormal = []
    const nLatitude = 50
    const nLongitude = 50
    const r = 1/2

    for (let lat = 0; lat <= nLatitude; lat++) {
        const theta = Math.PI * lat / nLatitude
        const thetaSin = Math.sin(theta)
        const thetaCos = Math.cos(theta)

        for (let long = 0; long <= nLongitude; long++) {
            const phi = 2 * Math.PI * long / nLongitude
            const phiSin = Math.sin(phi)
            const phiCos = Math.cos(phi)

            const x = phiCos * thetaSin
            const y = thetaCos
            const z = phiSin * thetaSin

            vposition.push(r * x, r * y, r * z)
            vnormal.push(x, y, z)
            // vcolor.push(Math.random(), Math.random(), Math.random(), 1)
            vcolor.push(0.5, 0.5, 0.5, 1)

            if (lat == nLatitude || long == nLongitude)
                continue

            const first = (lat * (nLongitude + 1)) + long
            const second = first + nLongitude + 1

            vindex.push(first)
            vindex.push(second)
            vindex.push(first + 1)

            vindex.push(second)
            vindex.push(second + 1)
            vindex.push(first + 1)
        }
    }

    const position = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, position)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vposition), gl.STATIC_DRAW)

    const color = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, color)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vcolor), gl.STATIC_DRAW)

    const index = gl.createBuffer()
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index)
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vindex), gl.STATIC_DRAW)

    const normal = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, normal)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vnormal), gl.STATIC_DRAW)

    return { position, color, index, normal, count: vindex.length }
}

async function skybox(gl) {
    const program = createShaderProgram(gl, await load("/shader/skybox-vertex.glsl"), await load("/shader/skybox-fragment.glsl"))

    return {
        program,
        buffer: skyboxBuffers(gl),
        texture: createCubemap(gl, {
            back: load('/skybox/back.jpg'),
            bottom: load('/skybox/bottom.jpg'),
            front: load('/skybox/front.jpg'),
            left: load('/skybox/left.jpg'),
            right: load('/skybox/right.jpg'),
            top: load('/skybox/top.jpg'),
        }),
        attrib: {
            vPosition: gl.getAttribLocation(program, 'avPosition'),
        },
        uniform: {
            xProjection: gl.getUniformLocation(program, 'uxProjection'),
            xModelView: gl.getUniformLocation(program, 'uxModelView'),
            sampler: gl.getUniformLocation(program, 'uSampler'),
        },
    }
}

function skyboxBuffers(gl) {
    const r = 100
    const vposition = [
        // front
        -r, -r, +r,
        +r, -r, +r,
        +r, +r, +r,
        -r, +r, +r,

        // back
        -r, -r, -r,
        -r, +r, -r,
        +r, +r, -r,
        +r, -r, -r,

        // top
        -r, +r, -r,
        -r, +r, +r,
        +r, +r, +r,
        +r, +r, -r,

        // bottom
        -r, -r, -r,
        +r, -r, -r,
        +r, -r, +r,
        -r, -r, +r,

        // right
        +r, -r, -r,
        +r, +r, -r,
        +r, +r, +r,
        +r, -r, +r,

        // left
        -r, -r, -r,
        -r, -r, +r,
        -r, +r, +r,
        -r, +r, -r,
    ]
    const vindex = [
        0,  1,  2,    0,  2,  3,  // front
        4,  5,  6,    4,  6,  7,  // back
        8,  9,  10,   8,  10, 11, // top
        12, 13, 14,   12, 14, 15, // bottom
        16, 17, 18,   16, 18, 19, // right
        20, 21, 22,   20, 22, 23, // left
    ]

    const position = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, position)
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vposition), gl.STATIC_DRAW)

    const index = gl.createBuffer()
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index)
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vindex), gl.STATIC_DRAW)

    return { position, index, count: 36 }
}

function drawScene(gl, main, skybox, up, pos, look) {
    gl.clearColor(0, 0, 0, 1)
    gl.clearDepth(1)
    gl.enable(gl.DEPTH_TEST)
    gl.depthFunc(gl.LEQUAL)

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    const fov = Math.PI / 4
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight
    const zNear = 0.1
    const zFar = 300

    const projection = mat4.perspective(mat4.create(), fov, aspect, zNear, zFar)
    const camera = mat4.lookAt(mat4.create(), pos, vec3.add(vec3.create(), pos, look), up)

    drawMain(gl, main, projection, camera)
    drawSkybox(gl, skybox, projection, camera)
}

function drawMain(gl, main, projection, camera) {
    gl.useProgram(main.program)

    gl.uniformMatrix4fv(main.uniform.xProjection, false, projection)
    gl.uniformMatrix4fv(main.uniform.xView, false, camera)
    gl.uniform3f(main.uniform.vAmbientLight, 0.3, 0.3, 0.3)
    gl.uniform3f(main.uniform.vDirectionalLight, 1, 1, 1)
    gl.uniform3fv(main.uniform.vLightDirection, vec3.normalize(vec3.create(), vec3.fromValues(-0.4, 0.5, -0.8)))

    if (!window.model)
        return

    const drawer = new Drawer(gl, main)
    window.model(drawer)
    drawer.render()
}

function drawSkybox(gl, skybox, projection, camera) {
    gl.bindBuffer(gl.ARRAY_BUFFER, skybox.buffer.position)
    gl.vertexAttribPointer(skybox.attrib.vPosition, 3, gl.FLOAT, false, 0, 0)
    gl.enableVertexAttribArray(skybox.attrib.vPosition)

    gl.useProgram(skybox.program)

    gl.uniformMatrix4fv(skybox.uniform.xProjection, false, projection)
    gl.uniformMatrix4fv(skybox.uniform.xModelView, false, camera)

    gl.activeTexture(gl.TEXTURE0)
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, skybox.texture)
    gl.uniform1i(skybox.uniform.sampler, 0)

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, skybox.buffer.index)
    gl.drawElements(gl.TRIANGLES, skybox.buffer.count, gl.UNSIGNED_SHORT, 0)
}
})();

0 // WKWebView requires a serializable 'return' value