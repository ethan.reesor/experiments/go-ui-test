const move = {}

window.addEventListener('keydown', e => {
    if (e.shiftKey || e.ctrlKey || e.altKey || e.metaKey)
        return

    if (e.target.tagName != 'BODY')
        return

    let key = e.key.toLowerCase()
    switch (key) {
        default:
            return

        case 'arrowup':
        case 'arrowdown':
        case 'arrowleft':
        case 'arrowright':
            key = key.slice(5)
            break

        case 'w':
        case 'a':
        case 's':
        case 'd':
        case 'q':
        case 'e':
            key = e.key
            break
    }

    move[key] = true
    console.log(e.target)
    e.preventDefault()
})

window.addEventListener('keyup', e => {
    let key = e.key.toLowerCase()
    switch (key) {
        default:
            return

        case 'arrowup':
        case 'arrowdown':
        case 'arrowleft':
        case 'arrowright':
            key = key.slice(5)
            break

        case 'w':
        case 'a':
        case 's':
        case 'd':
        case 'q':
        case 'e':
            key = e.key
            break
    }

    move[key] = false
    e.preventDefault()
})