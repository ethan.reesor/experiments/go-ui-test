function createShaderProgram(gl, vertexSrc, fragmentSrc) {
    const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexSrc)
    const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentSrc)

    const program = gl.createProgram()
    gl.attachShader(program, vertexShader)
    gl.attachShader(program, fragmentShader)
    gl.linkProgram(program)

    if (gl.getProgramParameter(program, gl.LINK_STATUS))
        return program

    const log = gl.getProgramInfoLog(program)
    gl.deleteProgram(program)

    throw new Error(`An error occurred while linking a program: ${log}`)
}

function createShader(gl, type, source) {
    const shader = gl.createShader(type)

    gl.shaderSource(shader, source)
    gl.compileShader(shader)

    if (gl.getShaderParameter(shader, gl.COMPILE_STATUS))
        return shader

    const log = gl.getShaderInfoLog(shader)
    gl.deleteShader(shader)

    throw new Error(`An error occurred while compiling a shader: ${log}`)
}

function createCubemap(gl, urls) {
    const texture = gl.createTexture()
    const level = 0
    const internalFormat = gl.RGBA
    const width = 1
    const height = 1
    const border = 0
    const srcFormat = gl.RGBA
    const srcType = gl.UNSIGNED_BYTE

	const tex = [
        [gl.TEXTURE_CUBE_MAP_POSITIVE_X, urls.right],
        [gl.TEXTURE_CUBE_MAP_NEGATIVE_X, urls.left],
        [gl.TEXTURE_CUBE_MAP_POSITIVE_Y, urls.top],
        [gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, urls.bottom],
        [gl.TEXTURE_CUBE_MAP_POSITIVE_Z, urls.front],
        [gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, urls.back],
    ]

    gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture)
	gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
    gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR)

    for (const [type, url] of tex) {
        gl.texImage2D(type, level, internalFormat, width, height, border, srcFormat, srcType, new Uint8Array([0, 255, 255, 255]))

        loadTexture(gl, url).then(apply => {
            gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture)
            apply(type, level, internalFormat, srcFormat, srcType)
        })
    }

    return texture
}

function createTexture(gl, url) {
    const texture = gl.createTexture()
    const level = 0
    const internalFormat = gl.RGBA
    const width = 1
    const height = 1
    const border = 0
    const srcFormat = gl.RGBA
    const srcType = gl.UNSIGNED_BYTE

    // set texture to blue while texture is loading
    gl.bindTexture(gl.TEXTURE_2D, texture)
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, width, height, border, srcFormat, srcType, new Uint8Array([0, 255, 255, 255]))

    loadTexture(gl, url).then(apply => {
        gl.bindTexture(gl.TEXTURE_2D, texture)
        const image = apply(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType)

        if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
            gl.generateMipmap(type)

        } else {
            gl.texParameteri(type, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
            gl.texParameteri(type, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
            gl.texParameteri(type, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
            gl.texParameteri(type, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
        }
    })
    return texture
}

function loadTexture(gl, url) {
    return new Promise(async r => {
        const image = new Image()
        image.onload = (() => r(apply.bind(null, image)))
        image.src = await url
    })

    function apply(image, type, level, internalFormat, srcFormat, srcType) {
        gl.texImage2D(type, level, internalFormat, srcFormat, srcType, image)
        return image
    }
}

function isPowerOf2(value) {
    return (value & (value - 1)) == 0
}