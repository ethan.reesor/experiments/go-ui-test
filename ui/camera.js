const camera = {
    horizontal: 0,
    vertical: 0,
    position: vec3.fromValues(0, 0, -6),

    rotateSpeed: 2,
    moveSpeed: 5,

    update(duration) {
        if (move.up)
            this.vertical += this.rotateSpeed * duration
        else if (move.down)
            this.vertical -= this.rotateSpeed * duration

        if (move.left)
            this.horizontal += this.rotateSpeed * duration
        else if (move.right)
            this.horizontal -= this.rotateSpeed * duration

        const look = vec3.fromValues(
            Math.cos(this.vertical) * Math.sin(this.horizontal),
            Math.sin(this.vertical),
            Math.cos(this.vertical) * Math.cos(this.horizontal)
        )

        const right = vec3.fromValues(
            -Math.cos(this.horizontal),
            0,
            Math.sin(this.horizontal)
        )

        const up = vec3.cross(vec3.create(), right, look)
        const pos = this.position

        if (move.w)
            vec3.scaleAndAdd(pos, pos, look, +this.moveSpeed*duration)
        else if (move.s)
            vec3.scaleAndAdd(pos, pos, look, -this.moveSpeed*duration)

        if (move.d)
            vec3.scaleAndAdd(pos, pos, right, +this.moveSpeed*duration)
        else if (move.a)
            vec3.scaleAndAdd(pos, pos, right, -this.moveSpeed*duration)

        if (move.q)
            vec3.scaleAndAdd(pos, pos, up, +this.moveSpeed*duration)
        else if (move.e)
            vec3.scaleAndAdd(pos, pos, up, -this.moveSpeed*duration)

        return { up, pos, look }
    }
}