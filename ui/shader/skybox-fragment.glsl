varying highp vec3 vTexDir;

uniform samplerCube uSampler;

void main(void) {
    gl_FragColor = textureCube(uSampler, vTexDir);
}