attribute vec4 avPosition;
attribute vec3 avNormal;
attribute vec4 avColor;

uniform mat4 uxNormal;
uniform mat4 uxModel;
uniform mat4 uxView;
uniform mat4 uxProjection;
uniform vec3 uvAmbientLight;
uniform vec3 uvDirectionalLight;
uniform vec3 uvLightDirection;

varying lowp vec4 vColor;
varying highp vec3 vLighting;

void main() {
    gl_Position = uxProjection * uxView * uxModel * avPosition;
    vColor = avColor;

    highp vec4 transformedNormal = uxNormal * vec4(normalize(avNormal), 1.0);

    highp float directional = max(dot(transformedNormal.xyz, uvLightDirection), 0.0);
    vLighting = uvAmbientLight + (uvDirectionalLight * directional);
}