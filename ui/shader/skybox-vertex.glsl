attribute vec4 avPosition;

uniform mat4 uxModelView;
uniform mat4 uxProjection;

varying highp vec3 vTexDir;

void main(void) {
    gl_Position = uxProjection * uxModelView * avPosition;
    vTexDir = vec3(avPosition);
}