package main

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/zserge/webview"
	app "gitlab.com/firelizzard/go-app"
)

//go:generate go run -tags=vfs assets_generate.go

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func unpack(raw string) []byte {
	b, err := base64.StdEncoding.DecodeString(raw)
	if err != nil {
		log.Fatal(err)
	}

	zip, err := gzip.NewReader(bytes.NewBuffer(b))
	if err != nil {
		log.Fatal(err)
	}
	defer zip.Close()

	c, err := ioutil.ReadAll(zip)
	if err != nil {
		log.Fatal(err)
	}

	return c
}

func eval(wv *webview.WebView, js string) {
	err := wv.Evaluate(js)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\nWhile evaluating:\n", err)
		fmt.Fprint(os.Stderr, js)
		fmt.Fprintln(os.Stderr)
		os.Exit(1)
	}
}

type Resources struct {
	wv *webview.WebView
}

func (r Resources) Get(id int, name string) {
	switch name {
	default:
		eval(r.wv, fmt.Sprintf("setResult(%d)", id))
	}
}

func main() {
	win := platformSetup(
		&app.Settings{
			AppName: "Modeler",
		},
		&app.WindowSettings{
			Title:        "Modeler",
			Width:        640,
			Height:       480 * 2,
			Resizeable:   true,
			Minimizeable: true,
			Closeable:    true,
		},
	)

	wvs := webview.NewSettings()
	wvs.EnableDebug()
	wvs.AddCustomScheme("rsc", http.FileServer(Assets))

	wv := webview.New(win, wvs)
	wv.Load("rsc://app/")

	go func() {
		_, err := wv.Bind("resources", Resources{wv})
		if err != nil {
			log.Fatal(err)
		}

		eval(wv, `
			;(() => {
				const { get } = resources
				resources.get = name => waitForResult(id => get(id, name))
			})()

			// function run(cb) {
			// 	window.addEventListener('load', cb)
			// }

			// run(() => window.run = x => x())
		`)

	}()

	win.Center()
	win.Focus()

	app.Run()
	app.Terminate()
}
