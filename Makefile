all: app

run: *.go
	go run .

clean:
	-rm -rf App.app ui/*.wasm

ui/main.wasm: ui/*.go
	# cd ui && tinygo build -target=wasm -no-debug -o main.wasm && cd ..
	cd ui && GOOS=js GOARCH=wasm go build -ldflags="-s -w" -o main.wasm && cd ..

assets_vfsdata.go: assets_generate.go ui/main.wasm ui/*.html ui/*.js ui/*.css ui/shader/* ui/skybox/*
	go generate

App.app/Contents/MacOS:
	mkdir -p App.app/Contents/MacOS

App.app/Contents/MacOS/App: *.go App.app/Contents/MacOS
	go build -o App.app/Contents/MacOS/App

App.app/Contents/Info.plist: Info.plist App.app/Contents/MacOS
	cp Info.plist App.app/Contents

app: App.app/Contents/MacOS/App App.app/Contents/Info.plist

.phony: all clean app run