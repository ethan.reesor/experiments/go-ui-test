package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
)

const goSrc = `
package main

const xibEncoded = "%s"
`

func main() {
	out, err := exec.Command("ibtool", "ui.xib", "--compile", "ui.xib.bin").CombinedOutput()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s", out)
		log.Fatal(err)
	}

	b, err := ioutil.ReadFile("ui.xib.bin")
	if err != nil {
		log.Fatal(err)
	}

	s := base64.RawStdEncoding.EncodeToString(b)

	err = ioutil.WriteFile("ui_darwin.go", []byte(fmt.Sprintf(goSrc[1:], s)), 0644)
	if err != nil {
		log.Fatal(err)
	}
}
