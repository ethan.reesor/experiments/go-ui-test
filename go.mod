module gitlab.com/ethan.reesor/experiments/go-ui-test

go 1.12

require (
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	github.com/zserge/webview v0.0.0-20191103184548-1a9ebffc2601
	gitlab.com/firelizzard/go-app v0.0.0-00010101000000-000000000000
	golang.org/x/tools v0.0.0-20191206204035-259af5ff87bd // indirect
)

replace github.com/zserge/webview => ../webview

replace gitlab.com/firelizzard/go-app => ../go-app
