// +build vfs

package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/shurcooL/httpfs/filter"
	"github.com/shurcooL/vfsgen"
)

func main() {
	var fs http.FileSystem
	fs = http.Dir("ui")
	fs = filter.Skip(fs, filter.FilesWithExtensions(".go", ".wasm"))
	fs = filter.Skip(fs, func(p string, _ os.FileInfo) bool {
		return strings.HasPrefix(path.Base(p), ".")
	})

	exec, err := ioutil.ReadFile(filepath.Join(runtime.GOROOT(), "misc", "wasm", "wasm_exec.js"))
	if err != nil {
		log.Fatalln(err)
	}

	resp, err := http.Get("https://mdn.github.io/webgl-examples/tutorial/gl-matrix.js")
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	glm, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	fs = &customFS{fs, exec, glm}

	err = vfsgen.Generate(fs, vfsgen.Options{
		PackageName:  "main",
		BuildTags:    "!vfs",
		VariableName: "Assets",
	})
	if err != nil {
		log.Fatalln(err)
	}
}

type customFS struct {
	main      http.FileSystem
	exec, glm []byte
}

func (fs *customFS) Open(name string) (http.File, error) {
	switch name {
	case "/":
		f, err := fs.main.Open("/")
		if err != nil {
			return f, err
		}
		return &customRoot{fs, f}, nil

	case "/wasm-exec.js":
		return &file{bytes.NewReader(fs.exec), name}, nil

	case "/gl-matrix.js":
		return &file{bytes.NewReader(fs.glm), name}, nil

	default:
		return fs.main.Open(name)
	}
}

type customRoot struct {
	*customFS
	http.File
}

func (r *customRoot) Readdir(count int) ([]os.FileInfo, error) {
	fi, err := r.File.Readdir(count)
	if err != nil {
		return fi, err
	}

	if count <= 0 || len(fi) < count {
		fi = append(fi, &fileInfo{"/wasm-exec.js", int64(len(r.exec))})
	}

	if count <= 0 || len(fi) < count {
		fi = append(fi, &fileInfo{"/gl-matrix.js", int64(len(r.glm))})
	}

	return fi, err
}

type fileInfo struct {
	name string
	size int64
}

func (fi *fileInfo) Name() string       { return fi.name }
func (fi *fileInfo) Size() int64        { return fi.size }
func (fi *fileInfo) Mode() os.FileMode  { return 0444 }
func (fi *fileInfo) ModTime() time.Time { return time.Now() }
func (fi *fileInfo) IsDir() bool        { return false }
func (fi *fileInfo) Sys() interface{}   { return nil }

type file struct {
	*bytes.Reader
	name string
}

func (f *file) Close() error { return nil }

func (f *file) Readdir(count int) ([]os.FileInfo, error) {
	return nil, nil
}

func (f *file) Stat() (os.FileInfo, error) {
	return &fileInfo{f.name, int64(f.Len())}, nil
}
